package com.devcamp.listen.repository;

import java.util.List;

import com.devcamp.listen.model.Customer;

public interface ICustomerLevel {
////c.id, 
//	c.phone_number, 
//	c.address, 
//	c.full_name, 
//	c.birthday, 
//	c.city, c.state, 
//	c.last_name, 
//	c.first_name, 
//	c.email, 
//	SUM(od.price_each * od.quantity_order) AS total_money 
//	
////	
	
	public long getId();
	public String getPhoneNumber();
	public String getAddress();
	public String getFullName();
	public String getBirthday();
	public String getCity();
	public String getState();
	public String getLastName();
	public String getFirstName();
	public String getEmail();
	public int getToTalMoney();
}
